import 'package:flutter/material.dart';
import 'package:flutter_door_buzzer/screens/buzz.dart';
import 'package:flutter_door_buzzer/screens/home.dart';
import 'package:flutter_door_buzzer/screens/signup.dart';

final routes = {
  '/' :       (BuildContext context) => new HomeScreen(),
  '/signup':    (BuildContext context) => new SignupScreen(),
  '/buzzer':    (BuildContext context) => new BuzzerScreen(),
};
