import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

postRequest({
  @required String url,
  Map<String, String> payload,
  headers
}) async {
  Map<String, String> baseHeaders = {
    'Content-Type': 'application/json'
  };
  if (headers != null) {
    baseHeaders.addAll(headers);
  }
  var body;
  if (payload != null) {
    body = jsonEncode(payload);
  }

  // TODO: manage HTTP errors
  var resp = await http.post(url, encoding: Utf8Codec(), headers: baseHeaders, body: body);
  if (resp.statusCode == 201) {
    // TODO: proper deserialization
    // return parseUserResponseData(resp.body);
    return jsonDecode(resp.body);
  } else {
    print("Error occurred: " + resp.statusCode.toString() + " " + resp.body);
    return resp.body;
  }
}
