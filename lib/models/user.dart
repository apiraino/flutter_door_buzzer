// import 'package:json_annotation/json_annotation.dart';

import 'dart:convert';

class User {

  final UserAuth user;
  final String password;

  User({ this.user, this.password });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      user: new UserAuth(
          email: json['email'],
          token: json['token']
      ),
      password: json['password']
    );
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["password"] = this.password;
    map["email"] = this.user.email;
    return map;
  }

}

class UserAuth {

  final int id;
  final String email;
  final String token;

  UserAuth({this.id, this.email, this.token});

  factory UserAuth.fromJson(Map<String, dynamic> json) {
    return UserAuth(
        id: json['id'] as int,
        email: json['email'],
        token: json['token']
    );
  }
}

UserAuth parseUserResponseData(String data) {
  final parsed = jsonDecode(data).cast<Map<String, dynamic>>();
  return parsed.map<UserAuth>((json) => UserAuth.fromJson(json));
}