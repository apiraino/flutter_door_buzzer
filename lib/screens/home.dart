import 'package:flutter/material.dart';
import 'package:flutter_door_buzzer/data/database_helper.dart';
import 'package:flutter_door_buzzer/models/user.dart';
import 'package:flutter_door_buzzer/screens/buzz.dart';

class HomeScreen extends StatefulWidget {
  // HomeScreen({Key key, this.title}) : super(key: key);
  final String title = "Welcome to CWRKNG";
  @override
  HomeScreenState createState() => HomeScreenState();
}

Future<User> getUser() async {
  var db = new DatabaseHelper();
  return await db.getUser();
}

class HomeScreenState extends State<HomeScreen> {
  BuildContext _ctx;

  void moveToSignup() {

    getUser().then((val) {
      if (val != null) {
        print("got user token: " + val.user.token.substring(0,10) + '...');
        print('moving to door buzzer');
        Navigator.push(
            _ctx,
            MaterialPageRoute(
              builder: (context) => BuzzerScreen(authToken: val.user.token),
            ));
      } else {
        print('moving to signup');
        // Navigator.of(_ctx).pushNamed("/signup");
        Navigator.of(_ctx).pushReplacementNamed("/signup");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            GestureDetector(
              onTap: moveToSignup,
                child: Container(
                  key: UniqueKey(),
                  color: Colors.blue,
                  width: 300.0,
                  child: Image.asset("assets/icons/android/playstore-icon.png"),
                ),
            ),

            // SizedBox(height: 100),

          ],
        ),
      ),
    );
  }
}
