import 'package:flutter/material.dart';
import 'package:flutter_door_buzzer/config.dart';
import 'package:flutter_door_buzzer/utils/network.dart';

class BuzzerScreen extends StatefulWidget {
  final String title = "Buzz the door";
  final String authToken;

  BuzzerScreen({this.authToken});

  @override
  BuzzerScreenState createState() => BuzzerScreenState(authToken: authToken);
}

class BuzzerScreenState extends State<BuzzerScreen> {
  BuildContext _ctx;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final String authToken;
  bool toggle;

  BuzzerScreenState({this.authToken});

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  void _buzzDoor() async {
    final authz = {
      'authorization': authToken
    };
    final url = CWRKNG_ENDPOINT + "/door/" + CWRKNG_DOOR;
    final respData = await postRequest(headers: authz, url: url);

    try {
      if (respData['status'] == 'OK') {
        _showSnackBar("Door opened!");
        _resetDoor();
      }
    } catch (e) {
      print("Door opening failed: " + respData.toString());
      _showSnackBar("Door failed to open!");
    }
  }

  void _resetDoor() {
    print("Resetting door...");
    setState(() {
      toggle = !toggle;
    });
  }

  @override
  void initState() {
    super.initState();
    toggle = true;
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;

    var g1 = GestureDetector(
      onTap: _buzzDoor,
      child: Container(
        key: UniqueKey(),
        color: Colors.blue,
        width: 200.0,
        child: Image.asset("assets/bell_ring.jpg"),
      ),
    );

    var g2 = GestureDetector(
      onTap: _resetDoor,
      child: Container(
        key: UniqueKey(),
        color: Colors.blue,
        width: 200.0,
        child: Image.asset("assets/door-open.png"),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      key: scaffoldKey,
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AnimatedCrossFade(
              duration: const Duration(seconds: 1),
              firstChild: g1,
              secondChild: g2,
              crossFadeState: toggle ? CrossFadeState.showFirst : CrossFadeState.showSecond,
            ),
          ],
        ),
      ),
    );
  }
}
