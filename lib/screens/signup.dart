import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_door_buzzer/config.dart';
import 'package:flutter_door_buzzer/data/database_helper.dart';
import 'package:flutter_door_buzzer/models/user.dart';
import 'package:flutter_door_buzzer/screens/buzz.dart';
import 'package:flutter_door_buzzer/utils/network.dart';
import 'package:flutter_door_buzzer/utils/rnd.dart';

class SignupScreen extends StatefulWidget {
  final String title = "Signup to CWRKNG";
  @override
  SignupScreenState createState() => SignupScreenState();
}

class SignupData {
  String email = '';
  String password = '';
}

class SignupScreenState extends State<SignupScreen> {
  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  SignupData signupData = new SignupData();

  void moveToBuzz(String authToken) {
    print('moving to buzzer');
    // go to screen keeping history
    // Navigator.of(_ctx).pushNamed("/buzzer");
    // go to screen discarding history
    // Navigator.of(_ctx).pushReplacementNamed("/buzzer");

    Navigator.push(
        _ctx,
        MaterialPageRoute(
          builder: (context) => BuzzerScreen(authToken: authToken),
        ));
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  void _doSignup() async {
    Map<String, String> payload = {};
    // get form and validate
    final form = formKey.currentState;
    if (form.validate()) {
      // TODO: loading spinner and callbacks
      // setState(() => _isLoading = true);
      form.save();
      payload = {
        'password': signupData.password,
        'email': signupData.email
      };
    } else {
      _showSnackBar("Form is invalid");
      return;
    }

    final url = CWRKNG_ENDPOINT + "/signup";
    // print(payload);

    var userAuthToken = "";
    final respData = await postRequest(payload: payload, url: url);
    try {
      userAuthToken = respData['user']['token'];
    } catch (e) {
      print("Signup failed: " + respData);
      _showSnackBar("Signup failed!");
    }

    if (userAuthToken != "") {
      _showSnackBar("Signup successful");
      var db = new DatabaseHelper();
      final userAuth = UserAuth.fromJson(respData['user']);
      final user = User(password: signupData.password,  user: userAuth);
      // final user = User.fromJson({'user': userAuth, 'password': signupData.password});
      await db.saveUser(user);
      moveToBuzz(userAuthToken);
    }
  }

  // controllers for form text controllers
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _pwdController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    _ctx = context;

    void _fillForm() {
      print("Triggered debug form fill");
      _emailController.text = generateRng() + "@domain.de";
      _pwdController.text = generateRng();
    }

    var signupBtn = new RaisedButton(
      onPressed: _doSignup,
      child: new Text("Proceed"),
      color: Colors.green[500]
    );

    var signupForm = new Column(
      children: <Widget>[

        new GestureDetector(
          onLongPress: _fillForm,
          child: new Text(
            "CWRKNG Signup",
            textScaleFactor: 2.0,
          ),
        ),

        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[

              new Padding(
                padding: const EdgeInsets.all(4.0),
                child: new TextFormField (
                  controller: _emailController,
                  onSaved: (String val) => this.signupData.email = val,
                  decoration: new InputDecoration(hintText: "Email"),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    value = value.trim();
                    String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regExp = new RegExp(pattern);
                    if (value.length == 0) {
                      return "Email ist erforderlich";
                    } else if (!regExp.hasMatch(value)) {
                      return "Falsche Email";
                    }
                    return null;
                  },
                ),
              ),

              new Padding(
                padding: const EdgeInsets.all(4.0),
                child: new TextFormField(
                  controller: _pwdController,
                  onSaved: (String val) => this.signupData.password = val,
                  decoration: new InputDecoration(hintText: "Passwort"),
                  obscureText: true,
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    value = value.trim();
                    if (value.length == 0) {
                      return "Passwort ist erforderlich";
                    }
                    return null;
                  },
                ),
              ),

            ],
          ),
        ),
        _isLoading ? new CircularProgressIndicator() : signupBtn
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );

    return Scaffold(
      appBar: null,
      key: scaffoldKey,
      body: new Container(
        child: new Center(
          child: new ClipRect(
            child: new BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: new Container(
                child: signupForm,
                height: 300.0,
                width: 300.0,
                decoration: new BoxDecoration(
                    color: Colors.grey.shade200.withOpacity(0.5)),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
